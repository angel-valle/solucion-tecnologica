from django.views import View
from ApiRest.models import DatosApiRest
from django.http import JsonResponse

class ListarUsuarios(View):
    def get(self, request):
        lista = DatosApiRest.objects.all()
        return JsonResponse(list(lista.values()), safe=False)

