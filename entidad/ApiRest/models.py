from django.db import models

# Create your models here.


class DatosApiRest(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    edad = models.IntegerField()
    correo = models.EmailField()
    identificacion = models.IntegerField()

 
    def __str__(self):
        return self.nombre

